// Fill out your copyright notice in the Description page of Project Settings.

#include "ShipParent.h"
#include "DrawDebugHelpers.h"

// Sets default values
AShipParent::AShipParent()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

void AShipParent::FuncLookUp(float FuncLookUpRate, UStaticMeshComponent* MeshToRotateLookUp, UStaticMeshComponent* MeshToRotateCameraLookUp)
{
	if (LookUpRate + FuncLookUpRate >= MaxMeshLookUp ||
		LookUpRate + FuncLookUpRate <= MaxMeshLookDown)
	{
		if (LookUpRate + FuncLookUpRate < 0)
		{
			MeshToRotateLookUp->SetRelativeRotation(FRotator(0, 0, MaxMeshLookDown));

			MeshToRotateCameraLookUp->AddLocalRotation(FRotator(FuncLookUpRate * -1, 0, 0));
			LookUpRate = LookUpRate + FuncLookUpRate;

			if (MeshToRotateCameraLookUp->GetRelativeRotation().Pitch >= MaxCameraLookUp)
			{
				MeshToRotateCameraLookUp->SetRelativeRotation(FRotator(MaxCameraLookUp, 0, 0));
				LookUpRate = MaxMeshLookDown - MaxCameraLookUp;
			}
			
		}
		else
		{
			MeshToRotateLookUp->SetRelativeRotation(FRotator(0, 0, MaxMeshLookUp));
			LookUpRate = MaxMeshLookUp;
		}
	}
	else
	{
		MeshToRotateLookUp->AddLocalRotation(FRotator(0, 0, FuncLookUpRate));
		LookUpRate = LookUpRate + FuncLookUpRate;
	}
}

void AShipParent::FuncLookAround(float FuncLookAroundRate, UStaticMeshComponent* MeshToRotateLookAround)
{
	MeshToRotateLookAround->AddLocalRotation(FRotator(0, FuncLookAroundRate, 0));

	if (TurnRate + FuncLookAroundRate > 360)
		TurnRate = (TurnRate + FuncLookAroundRate - 360);

	if(TurnRate + FuncLookAroundRate < -360)
		TurnRate = (TurnRate + FuncLookAroundRate + 360);

	if (TurnRate + FuncLookAroundRate < 360 || TurnRate + FuncLookAroundRate > -360)
		TurnRate = (TurnRate + FuncLookAroundRate);
	
		
}

void AShipParent::FuncTurretRotation(float FuncTurnRate, float FuncTurretRotationSpeed, float FuncMaxTurretRotation, const TArray<UStaticMeshComponent*>& MeshesToRotateLookAround)
{
	float CurrentTurnRate = 0;
	float CurrentTurretRotation = 0;
	
	// Define camera rotation between 0 and 360
	if (FuncTurnRate < 0)
	{
		CurrentTurnRate = 360 - abs(FuncTurnRate);
	}
	else
	{
		CurrentTurnRate = FuncTurnRate;
	}

	if (FuncMaxTurretRotation == 0)
	{
		for (UStaticMeshComponent* CurrentMesh : MeshesToRotateLookAround)
		{
			if (CurrentMesh == nullptr)
			{
				UE_LOG(LogTemp, Warning, TEXT("Empty turret array"));
				break;
			}
			// Define mesh rotation between 0 and 360
			if (CurrentMesh->GetRelativeRotation().Yaw < 0)
			{
				CurrentTurretRotation = 360 - abs(CurrentMesh->GetRelativeRotation().Yaw);
			}
			else
			{
				CurrentTurretRotation = CurrentMesh->GetRelativeRotation().Yaw;
			}
			//UE_LOG(LogTemp, Warning, TEXT("Output: %f, %f, %f"), CurrentTurnRate, FuncTurnRate, FuncTurretRotationSpeed);
			// Set turret rotation to camera rotation 
			if (abs(CurrentTurnRate - CurrentTurretRotation) < FuncTurretRotationSpeed)
			{
				CurrentMesh->SetRelativeRotation(FRotator(0, CurrentTurnRate, 0));
			}
			else
			{
				bool FromLeftToRightCircle = CurrentTurnRate + 360 - CurrentTurretRotation < CurrentTurretRotation - CurrentTurnRate;
				bool FromRigthToLeftCircle = 360 - CurrentTurnRate + CurrentTurretRotation < CurrentTurnRate - CurrentTurretRotation;

				if ((CurrentTurnRate > CurrentTurretRotation || FromLeftToRightCircle) &&
					(CurrentTurnRate > CurrentTurretRotation && FromRigthToLeftCircle) == false)
				{
					CurrentMesh->AddRelativeRotation(FRotator(0, FuncTurretRotationSpeed, 0));
				}
				else
				{
					CurrentMesh->AddRelativeRotation(FRotator(0, FuncTurretRotationSpeed * -1, 0));
				}
			}
		}
	}
	if (FuncMaxTurretRotation > 0)
	{
		for (UStaticMeshComponent* CurrentMesh : MeshesToRotateLookAround)
		{
			if (CurrentMesh == nullptr)
			{
				UE_LOG(LogTemp, Warning, TEXT("Empty turret array"));
				break;
			}
			// Define mesh rotation between 0 and 360
			if (CurrentMesh->GetRelativeRotation().Yaw < 0)
			{
				CurrentTurretRotation = 360 - abs(CurrentMesh->GetRelativeRotation().Yaw);
			}
			else
			{
				CurrentTurretRotation = CurrentMesh->GetRelativeRotation().Yaw;
			}

			if (abs(CurrentTurnRate - CurrentTurretRotation) < FuncTurretRotationSpeed)
			{
				CurrentMesh->SetRelativeRotation(FRotator(0, CurrentTurnRate, 0));
			}
			else
			{
				if ((CurrentTurnRate > CurrentTurretRotation && CurrentTurretRotation < FuncMaxTurretRotation && CurrentTurnRate < 180 && CurrentTurretRotation < 180) ||
					((CurrentTurnRate > 180 && CurrentTurretRotation > 180) && CurrentTurnRate > CurrentTurretRotation) ||
					(CurrentTurnRate < 180 && CurrentTurretRotation > 180))
				{
					CurrentMesh->AddRelativeRotation(FRotator(0, FuncTurretRotationSpeed, 0));
				}

				if ((CurrentTurnRate < CurrentTurretRotation && CurrentTurretRotation <= FuncMaxTurretRotation + 0.01 && CurrentTurnRate < 180) ||
					(CurrentTurnRate > 180 && CurrentTurretRotation < 180) ||
					((CurrentTurnRate > 180 && CurrentTurretRotation > 180) && CurrentTurnRate < CurrentTurretRotation))
				{
					CurrentMesh->AddRelativeRotation(FRotator(0, FuncTurretRotationSpeed * -1, 0));
				}
			}
			
			// Define mesh rotation between 0 and 360
			if (CurrentMesh->GetRelativeRotation().Yaw < 0)
			{
				CurrentTurretRotation = 360 - abs(CurrentMesh->GetRelativeRotation().Yaw);
			}
			else
			{
				CurrentTurretRotation = CurrentMesh->GetRelativeRotation().Yaw;
			}

			if (CurrentTurretRotation < 360 - FuncMaxTurretRotation && CurrentTurnRate > 180 && CurrentTurretRotation > 180)
			{
				CurrentMesh->SetRelativeRotation(FRotator(0, 360 - FuncMaxTurretRotation, 0));
			}
			if (CurrentTurretRotation > FuncMaxTurretRotation && CurrentTurnRate < 180 && CurrentTurretRotation < 180)
			{
				CurrentMesh->SetRelativeRotation(FRotator(0, FuncMaxTurretRotation, 0));
			}
		}
	}
	if (FuncMaxTurretRotation < 0)
	{
		for (UStaticMeshComponent* CurrentMesh : MeshesToRotateLookAround)
		{
			if (CurrentMesh == nullptr)
			{
				UE_LOG(LogTemp, Warning, TEXT("Empty turret array"));
				break;
			}
			// Define mesh rotation between 0 and 360
			if (CurrentMesh->GetRelativeRotation().Yaw < 0)
			{
				CurrentTurretRotation = 360 - abs(CurrentMesh->GetRelativeRotation().Yaw);
			}
			else
			{
				CurrentTurretRotation = CurrentMesh->GetRelativeRotation().Yaw;
			}

			if (abs(CurrentTurnRate - CurrentTurretRotation) < FuncTurretRotationSpeed)
			{
				CurrentMesh->SetRelativeRotation(FRotator(0, CurrentTurnRate, 0));
			}
			else
			{
				if (CurrentTurnRate > CurrentTurretRotation)
					CurrentMesh->AddRelativeRotation(FRotator(0, FuncTurretRotationSpeed, 0));
				if (CurrentTurnRate < CurrentTurretRotation)
					CurrentMesh->AddRelativeRotation(FRotator(0, FuncTurretRotationSpeed * -1, 0));
			}	
				
			// Define mesh rotation between 0 and 360
			if (CurrentMesh->GetRelativeRotation().Yaw < 0)
			{
				CurrentTurretRotation = 360 - abs(CurrentMesh->GetRelativeRotation().Yaw);
			}
			else
			{
				CurrentTurretRotation = CurrentMesh->GetRelativeRotation().Yaw;
			}

			if (CurrentTurretRotation < 180 - FuncMaxTurretRotation*-1 && CurrentTurretRotation < 180)
			{
				CurrentMesh->SetRelativeRotation(FRotator(0, 180 - FuncMaxTurretRotation * -1, 0));
			}
			if (CurrentTurretRotation > 180 + FuncMaxTurretRotation*-1 && CurrentTurretRotation > 180)
			{
				CurrentMesh->SetRelativeRotation(FRotator(0, 180 + FuncMaxTurretRotation * -1, 0));
			}
		}
	}
}

void AShipParent::FuncRightBoardTurretRotation(float FuncTurnRate, float FuncTurretRotationSpeed, const TArray<UStaticMeshComponent*>& MeshesToRotateLookAround)
{
	float CurrentTurnRate = 0;
	float CurrentTurretRotation = 0;

	// Define camera rotation between 0 and 360
	if (FuncTurnRate < 0)
	{
		CurrentTurnRate = 360 - abs(FuncTurnRate);
	}
	else
	{
		CurrentTurnRate = FuncTurnRate;
	}

	for (UStaticMeshComponent* CurrentMesh : MeshesToRotateLookAround)
	{
		if (CurrentMesh == nullptr)
		{
			UE_LOG(LogTemp, Warning, TEXT("Empty turret array"));
			break;
		}
		// Define mesh rotation between 0 and 360
		if (CurrentMesh->GetRelativeRotation().Yaw < 0)
		{
			CurrentTurretRotation = 360 - abs(CurrentMesh->GetRelativeRotation().Yaw);
		}
		else
		{
			CurrentTurretRotation = CurrentMesh->GetRelativeRotation().Yaw;
		}

		if (abs(CurrentTurnRate - CurrentTurretRotation) < FuncTurretRotationSpeed)
		{
			CurrentMesh->SetRelativeRotation(FRotator(0, CurrentTurnRate, 0));
		}
		else
		{
			if (CurrentTurnRate > CurrentTurretRotation && CurrentTurnRate <= 270)
				CurrentMesh->AddRelativeRotation(FRotator(0, FuncTurretRotationSpeed, 0));
			if (CurrentTurnRate < CurrentTurretRotation || (CurrentTurnRate > CurrentTurretRotation && CurrentTurnRate > 270))
				CurrentMesh->AddRelativeRotation(FRotator(0, FuncTurretRotationSpeed * -1, 0));
		}

		// Define mesh rotation between 0 and 360
		if (CurrentMesh->GetRelativeRotation().Yaw < 0)
		{
			CurrentTurretRotation = 360 - abs(CurrentMesh->GetRelativeRotation().Yaw);
		}
		else
		{
			CurrentTurretRotation = CurrentMesh->GetRelativeRotation().Yaw;
		}

		if (CurrentTurretRotation > 170)
		{
			CurrentMesh->SetRelativeRotation(FRotator(0, 170, 0));
		}
		if (CurrentTurretRotation < 10)
		{
			CurrentMesh->SetRelativeRotation(FRotator(0, 10, 0));
		}
	}
}

void AShipParent::FuncLeftBoardTurretRotation(float FuncTurnRate, float FuncTurretRotationSpeed, const TArray<UStaticMeshComponent*>& MeshesToRotateLookAround)
{
	float CurrentTurnRate = 0;
	float CurrentTurretRotation = 0;

	// Define camera rotation between 0 and 360
	if (FuncTurnRate < 0)
	{
		CurrentTurnRate = 360 - abs(FuncTurnRate);
	}
	else
	{
		CurrentTurnRate = FuncTurnRate;
	}

	for (UStaticMeshComponent* CurrentMesh : MeshesToRotateLookAround)
	{
		if (CurrentMesh == nullptr)
		{
			UE_LOG(LogTemp, Warning, TEXT("Empty turret array"));
			break;
		}
		// Define mesh rotation between 0 and 360
		if (CurrentMesh->GetRelativeRotation().Yaw < 0)
		{
			CurrentTurretRotation = 360 - abs(CurrentMesh->GetRelativeRotation().Yaw);
		}
		else
		{
			CurrentTurretRotation = CurrentMesh->GetRelativeRotation().Yaw;
		}

		if (abs(CurrentTurnRate - CurrentTurretRotation) < FuncTurretRotationSpeed)
		{
			CurrentMesh->SetRelativeRotation(FRotator(0, CurrentTurnRate, 0));
		}
		else
		{
			if (CurrentTurnRate > CurrentTurretRotation || (CurrentTurnRate < CurrentTurretRotation && CurrentTurnRate <= 90))
				CurrentMesh->AddRelativeRotation(FRotator(0, FuncTurretRotationSpeed, 0));
			if (CurrentTurnRate < CurrentTurretRotation && CurrentTurnRate > 90)
				CurrentMesh->AddRelativeRotation(FRotator(0, FuncTurretRotationSpeed * -1, 0));
		}

		// Define mesh rotation between 0 and 360
		if (CurrentMesh->GetRelativeRotation().Yaw < 0)
		{
			CurrentTurretRotation = 360 - abs(CurrentMesh->GetRelativeRotation().Yaw);
		}
		else
		{
			CurrentTurretRotation = CurrentMesh->GetRelativeRotation().Yaw;
		}

		if (CurrentTurretRotation > 350)
		{
			CurrentMesh->SetRelativeRotation(FRotator(0, 350, 0));
		}
		if (CurrentTurretRotation < 190)
		{
			CurrentMesh->SetRelativeRotation(FRotator(0, 190, 0));
		}
	}
}

void AShipParent::FuncCannonLookUp(float FuncLookUpRate, float FuncCannonRotationSpeed,
	const FHitResult& HitResult, float ProjectileSpeed, const TArray<UStaticMeshComponent*>& MeshesToRotateLookUp, float FuncMaxCannonLookUp, float FuncMaxCannonLookDown)
{	
	float ShootingDistance = 0;
	bIsMainCannonCanShoot = true;	

	if (HitResult.bBlockingHit)
	{
		ShootingDistance = sqrt(pow(CameraForwardTraceHit.Location.X - this->GetActorLocation().X, 2) +
			pow(CameraForwardTraceHit.Location.Y - this->GetActorLocation().Y, 2));
		FuncLookUpRate = CalculateShootingAngle(ProjectileSpeed, ShootingDistance);
		if (ShootingDistance < MinimalMainGunShootDistance)
			FuncLookUpRate = FuncMaxCannonLookDown;
		//UE_LOG(LogTemp, Warning, TEXT("Output: %f"), FuncLookUpRate);
	}
	// � ������ ���������� ��������� �� ������ ������������ ��������� 
	else
	{
		FuncLookUpRate = FuncMaxCannonLookUp;
	}

	//float CurrentShootingDistance = (ProjectileSpeed * sin(2 * MeshesToRotateLookUp[0]->GetRelativeRotation().Roll)) / GetWorld()->GetGravityZ();
	UE_LOG(LogTemp, Warning, TEXT("Output: %f, %f, %f"), ShootingDistance,  MinimalMainGunShootDistance, MaximalMainGunShootDistance);
	if (ShootingDistance > MaximalMainGunShootDistance || int(FuncLookUpRate) != int(MeshesToRotateLookUp[0]->GetRelativeRotation().Roll))
		bIsMainCannonCanShoot = false;

	for (UStaticMeshComponent* CurrentMesh : MeshesToRotateLookUp)
	{
		if (CurrentMesh == nullptr)
		{
			UE_LOG(LogTemp, Warning, TEXT("Empty cannon array"));
			break;
		}
		if (FuncLookUpRate > CurrentMesh->GetRelativeRotation().Roll)
		{
			if (FuncLookUpRate - CurrentMesh->GetRelativeRotation().Roll < FuncCannonRotationSpeed)
				CurrentMesh->SetRelativeRotation(FRotator(0, 0, FuncLookUpRate));
			else
				CurrentMesh->AddRelativeRotation(FRotator(0, 0, FuncCannonRotationSpeed));
		}
		if (CurrentMesh->GetRelativeRotation().Roll >= FuncMaxCannonLookUp)
		{
			CurrentMesh->SetRelativeRotation(FRotator(0, 0, FuncMaxCannonLookUp));
		}
		if (FuncLookUpRate < CurrentMesh->GetRelativeRotation().Roll)
		{
			if (CurrentMesh->GetRelativeRotation().Roll - FuncLookUpRate < FuncCannonRotationSpeed)
				CurrentMesh->SetRelativeRotation(FRotator(0, 0, FuncLookUpRate));
			else
				CurrentMesh->AddRelativeRotation(FRotator(0, 0, FuncCannonRotationSpeed*-1));
		}
		if (CurrentMesh->GetRelativeRotation().Roll <= FuncMaxCannonLookDown)
		{
			CurrentMesh->SetRelativeRotation(FRotator(0, 0, FuncMaxCannonLookDown));
		}
	}
}

void AShipParent::FuncForwardLineTrace(FHitResult& OutHit, int Multiplier, ECollisionChannel TraceChannel, UStaticMeshComponent* MeshToMakeTrace, const TArray<AActor*>& ActorsToIgnore, bool bMakeDebugLine)
{
	if (MeshToMakeTrace != nullptr)
	{
		FCollisionQueryParams CollisionParams;
		CollisionParams.bTraceComplex = true;
		CollisionParams.AddIgnoredActors(ActorsToIgnore);
		GetWorld()->LineTraceSingleByChannel(OutHit, MeshToMakeTrace->GetComponentLocation(),
			MeshToMakeTrace->GetComponentLocation() + MeshToMakeTrace->GetForwardVector() * Multiplier,
			TraceChannel, CollisionParams);
		
		if (bMakeDebugLine)
		{
			if (OutHit.bBlockingHit)
			{
				DrawDebugLine(GetWorld(), MeshToMakeTrace->GetComponentLocation(),
					OutHit.Location, FColor::Red, false, 1, 0, 1);
				DrawDebugLine(GetWorld(), OutHit.Location,
					MeshToMakeTrace->GetComponentLocation() + MeshToMakeTrace->GetForwardVector()
					* Multiplier, FColor::Green, false, 1, 0, 1);
			}
			else
			{
				DrawDebugLine(GetWorld(), MeshToMakeTrace->GetComponentLocation(),
					MeshToMakeTrace->GetComponentLocation() + MeshToMakeTrace->GetForwardVector()
					* Multiplier, FColor::Red, false, 1, 0, 1);
			}
		}
	}
	else 
		UE_LOG(LogTemp, Warning, TEXT("No trace mesh"));
}

void AShipParent::FuncShipMovment(UStaticMeshComponent* MeshToAddForce)
{
	if (MeshToAddForce->GetComponentVelocity().Size() < abs(CurrentSpeedLevel * SpeedStep))
	{
		MeshToAddForce->AddForce(this->GetActorRotation().RotateVector(FVector(0, CurrentSpeedLevel * ForwardMovmentForceValue, 0)), TEXT("None"), true);
	}
	if (CurrentSpeedLevel != 0)
	{
		MeshToAddForce->AddForceAtLocation(this->GetActorRotation().RotateVector(FVector(AngularMovmentForceValue, 0, 0) * CurrentTurnLevel)
			, this->GetActorLocation() + FRotator(0, 90, 0).RotateVector(MeshToAddForce->GetForwardVector()) * 10000 * ShipSizeMultiplier, TEXT("None"));
	}
}

float AShipParent::CalculateShootingAngle(float ProjectileSpeed, float Distance)
{
	return ((0.5f * asinf((Distance * abs(GetWorld()->GetGravityZ()))/pow(ProjectileSpeed, 2)))*180.0f/PI);
}


// Called when the game starts or when spawned
void AShipParent::BeginPlay()
{
	Super::BeginPlay();
	MinimalMainGunShootDistance = MainCannonProjectileSpeed * sqrt(2 * 
		MainTurretMassForward[0]->GetRelativeLocation().Z / abs(GetWorld()->GetGravityZ()));
	MaximalMainGunShootDistance = abs(pow(MainCannonProjectileSpeed,2) * sin(2.0f * MainCannonMaxLookUp * 180.0f / PI) / (GetWorld()->GetGravityZ()*2.5));
}

// Called every frame
void AShipParent::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	// Calculate variable to ignore fps dilation
	FrameDelta = DeltaTime / (1.0 / 60.0);
	
	FuncForwardLineTrace(CameraForwardTraceHit, 888888, ECollisionChannel::ECC_Visibility, ForwardTraceMesh, { this },false);
	// Ship movment
	FuncShipMovment(ShipBody);
	// Turret rotations 
	FuncTurretRotation(TurnRate, MainTurretRotationSpeed * FrameDelta, MainCannonMaxLookAround, MainTurretMassForward);
	FuncTurretRotation(TurnRate, MainTurretRotationSpeed * FrameDelta, MainCannonMaxLookAround*-1, MainTurretMassBackward);
	FuncRightBoardTurretRotation(TurnRate, LittleTurretRotationSpeed * FrameDelta, RightBoardTurretMass);
	FuncLeftBoardTurretRotation(TurnRate, LittleTurretRotationSpeed * FrameDelta, LeftBoardTurretMass);
	FuncCannonLookUp(LookUpRate, MainCannonLookUpSpeed, CameraForwardTraceHit, MainCannonProjectileSpeed, MainCannonMass, MainCannonMaxLookUp, MainCannonMaxLookDown);

	// Torpedo appparat rotation. If have got it
	if (bHasTorpedoApparat)
	{
		FuncTurretRotation(TurnRate, MainTorpedoApparatRotationSpeed * FrameDelta, MainCannonMaxLookAround, MainTorpedoApparatsMassForward);
		FuncTurretRotation(TurnRate, MainTorpedoApparatRotationSpeed * FrameDelta, MainCannonMaxLookAround * -1, MainTorpedoApparatsMassBackward);
		FuncRightBoardTurretRotation(TurnRate, MainTorpedoApparatRotationSpeed * FrameDelta, RightBoardTorpedoApparatsMass);
		FuncLeftBoardTurretRotation(TurnRate, MainTorpedoApparatRotationSpeed * FrameDelta, LeftBoardTorpedoApparatsMass);
	}
}

// Called to bind functionality to input
void AShipParent::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}


// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Runtime/Engine/Public/EngineGlobals.h"

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "ShipParent.generated.h"


UCLASS()
class WARSHIPS_API AShipParent : public APawn
{
	GENERATED_BODY()

public:
	float MinimalMainGunShootDistance;
	float MaximalMainGunShootDistance;
public:
	// Sets default values for this pawn's properties
	AShipParent();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MyVars")
	TArray<UStaticMeshComponent*> MainTurretMassForward;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MyVars")
	TArray<UStaticMeshComponent*> MainTurretMassBackward;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MyVars")
	TArray<UStaticMeshComponent*> RightBoardTurretMass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MyVars")
	TArray<UStaticMeshComponent*> LeftBoardTurretMass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MyVars")
	TArray<UStaticMeshComponent*> MainTorpedoApparatsMassForward;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MyVars")
	TArray<UStaticMeshComponent*> MainTorpedoApparatsMassBackward;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MyVars")
	TArray<UStaticMeshComponent*> RightBoardTorpedoApparatsMass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MyVars")
	TArray<UStaticMeshComponent*> LeftBoardTorpedoApparatsMass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MyVars")
	TArray<UStaticMeshComponent*> MainCannonMass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MyVars")
	UStaticMeshComponent* ShipBody;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Specs")
	bool bHasTorpedoApparat;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dynamic")
	float LookUpRate;

	/** Variable to Limit camera rotation */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MyVars")
	float MaxCameraLookUp;

	/** Variable to Limit Vertical rotator */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MyVars")
	float MaxMeshLookUp;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MyVars")
	float MaxMeshLookDown;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dynamic")
	float TurnRate;

	/** Variable to ignore world time delta */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dynamic")
	float FrameDelta;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Specs")
	float MainTurretRotationSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Specs")
	float MainTorpedoApparatRotationSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Specs")
	float MainCannonLookUpSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Specs")
	float MainCannonMaxLookUp;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Specs")
	float MainCannonMaxLookDown;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Specs")
	float MainCannonMaxLookAround;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Specs")
	float LittleTurretRotationSpeed;

	/** Variable to set current max ship speed */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dynamic")
	int CurrentSpeedLevel;

	/** Variable to set speed level multiplier */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Specs")
	float SpeedStep;

	/** Variable to set current turn 
	-1 - left
	0 - forward 
	+1 - right */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dynamic")
	int CurrentTurnLevel;

	/** Variable to set position to add angular force */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Specs")
	float ShipSizeMultiplier;

	/** Variable to set forward force value */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Specs")
	float ForwardMovmentForceValue;

	/** Variable to set angular force value */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Specs")
	float AngularMovmentForceValue;

	/** Variable to get FHitResult from camera forward trace*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dynamic")
	FHitResult CameraForwardTraceHit;

	/** Variable to get camera(but its not a camera) forward trace mesh */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dynamic")
	UStaticMeshComponent* ForwardTraceMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Specs")
	float MainCannonProjectileSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dynamic")
	bool bIsMainCannonCanShoot;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	UFUNCTION(BlueprintCallable, Category = "MyVars")
	void FuncLookUp(float FuncLookUpRate, UStaticMeshComponent* MeshToRotateLookUp, UStaticMeshComponent* MeshToRotateCameraLookUp);

	UFUNCTION(BlueprintCallable, Category = "MyVars")
	void FuncLookAround(float FuncLookAroundRate, UStaticMeshComponent* MeshToRotateLookAround);

	UFUNCTION(BlueprintCallable, Category = "MyVars")
	void FuncTurretRotation(float FuncTurnRate, float FuncTurretRotationSpeed, float FuncMaxTurretRotation, const TArray<UStaticMeshComponent*>& MeshesToRotateLookAround);

	UFUNCTION(BlueprintCallable, Category = "MyVars")
	void FuncRightBoardTurretRotation(float FuncTurnRate, float FuncTurretRotationSpeed, const TArray<UStaticMeshComponent*>& MeshesToRotateLookAround);

	UFUNCTION(BlueprintCallable, Category = "MyVars")
	void FuncLeftBoardTurretRotation(float FuncTurnRate, float FuncTurretRotationSpeed, const TArray<UStaticMeshComponent*>& MeshesToRotateLookAround);

	UFUNCTION(BlueprintCallable, Category = "MyVars")
	void FuncCannonLookUp(float FuncLookUpRate, float FuncCannonRotationSpeed,
		const FHitResult& HitResult, float ProjectileSpeed, const TArray<UStaticMeshComponent*>& MeshesToRotateLookUp, float FuncMaxCannonLookUp, float FuncMaxCannonLookDown);

	UFUNCTION(BlueprintCallable, Category = "MyVars")
	void FuncForwardLineTrace(FHitResult& OutHit, int Multiplier, ECollisionChannel TraceChannel, UStaticMeshComponent* MeshToMakeTrace, const TArray<AActor*>& ActorsToIgnore, bool bMakeDebugLine);

	UFUNCTION(BlueprintCallable, Category = "MyVars")
	void FuncShipMovment(UStaticMeshComponent* MeshToAddForce);

	UFUNCTION(BlueprintCallable, Category = "MyVars")
	float CalculateShootingAngle(float ProjectileSpeed, float Distance);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
